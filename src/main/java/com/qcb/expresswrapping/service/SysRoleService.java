package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.SysVo.SysRole;

import java.util.List;

public interface SysRoleService extends IService<SysRole> {

    List<SysRole> listRolesByUserId(Long id);
}
