package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.SysVo.SysRoleMenu;

public interface SysRoleMenuService extends IService<SysRoleMenu> {
}
