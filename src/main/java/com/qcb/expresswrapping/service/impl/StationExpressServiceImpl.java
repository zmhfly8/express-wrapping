package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.StationExpress;
import com.qcb.expresswrapping.mapper.StationExpressMapper;
import com.qcb.expresswrapping.service.StationExpressService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-11-17:20
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class StationExpressServiceImpl extends ServiceImpl<StationExpressMapper, StationExpress> implements StationExpressService {
}
