package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.SysVo.SysUserRole;
import com.qcb.expresswrapping.mapper.SysUserRoleMapper;
import com.qcb.expresswrapping.service.SysUserRoleService;
import org.springframework.stereotype.Service;

@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements IService<SysUserRole>, SysUserRoleService {

}
