package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.StationInfo;
import com.qcb.expresswrapping.mapper.StationInfoMapper;
import com.qcb.expresswrapping.service.StationInfoService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-11-23:06
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class StationInfoServiceImpl extends ServiceImpl<StationInfoMapper, StationInfo> implements StationInfoService {
}
