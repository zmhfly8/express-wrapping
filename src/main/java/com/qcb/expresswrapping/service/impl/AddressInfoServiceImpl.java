package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.AddressInfo;
import com.qcb.expresswrapping.mapper.AddressInfoMapper;
import com.qcb.expresswrapping.service.AddressInfoService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-10-22:29
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class AddressInfoServiceImpl extends ServiceImpl<AddressInfoMapper, AddressInfo> implements AddressInfoService {
}
