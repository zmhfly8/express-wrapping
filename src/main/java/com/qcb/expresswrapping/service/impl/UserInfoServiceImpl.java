package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.UserInfo;
import com.qcb.expresswrapping.mapper.UserInfoMapper;
import com.qcb.expresswrapping.service.UserInfoService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-10-19:41
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {
}
