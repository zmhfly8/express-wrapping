package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.LogisticsInfo;
import com.qcb.expresswrapping.mapper.LogisticsInfoMapper;
import com.qcb.expresswrapping.service.LogisticsInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-12-23:08
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class LogisticsInfoServiceImpl extends ServiceImpl<LogisticsInfoMapper, LogisticsInfo> implements LogisticsInfoService {
    @Override
    public List<LogisticsInfo> listLogisiticsByOrderId(String orderId) {
        List<LogisticsInfo> logisticsInfos = this.list(new QueryWrapper<LogisticsInfo>().inSql("logisticsId",
                "SELECT logisticsId FROM order_logistics WHERE orderId = '" + orderId + "'"));
        return logisticsInfos;
    }
}
