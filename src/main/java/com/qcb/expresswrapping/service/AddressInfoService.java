package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.AddressInfo;

public interface AddressInfoService extends IService<AddressInfo> {
}
