package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.SysVo.SysUser;

public interface SysUserService extends IService<SysUser> {

    // 获取登录用户信息
    SysUser getByUsername(String username);

    // 获取用户所拥有的权限
    String getUserAuthority(Long userId);

    // 清除redis缓存里的用户权限信息
    void clearUserAuthorityInfo(String username);

    void clearUserAuthorityInfoByRoleId(Long roleId);

    void clearUserAuthorityInfoByMenuId(Long menuId);
}
