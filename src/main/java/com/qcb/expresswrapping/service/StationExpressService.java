package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.StationExpress;

public interface StationExpressService extends IService<StationExpress> {
}
