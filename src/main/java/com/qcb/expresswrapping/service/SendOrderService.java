package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.SendOrder;

public interface SendOrderService extends IService<SendOrder> {
}
