package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.UserInfo;
import org.springframework.stereotype.Service;

public interface UserInfoService extends IService<UserInfo> {
}
