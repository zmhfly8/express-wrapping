package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.StationInfo;

public interface StationInfoService extends IService<StationInfo> {
}
