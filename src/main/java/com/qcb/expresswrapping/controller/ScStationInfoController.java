package com.qcb.expresswrapping.controller;

import com.qcb.expresswrapping.common.Result;
import com.qcb.expresswrapping.entity.StationInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-11-23:04
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/sc/station")
public class ScStationInfoController extends BaseController{

    @GetMapping("/info")
    @PreAuthorize("hasAuthority('sc:station:info')")
    public Result info(Principal principal) {
        StationInfo stationInfo = scStationInfoService.getById(sysUserService.getByUsername(principal.getName()).getId());
        Assert.notNull(stationInfo, "找不到该用户信息");
        return Result.success(stationInfo);
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sc:station:save')")
    public Result save(@Validated @RequestBody StationInfo stationInfo, Principal principal) {
        stationInfo.setAccountId(sysUserService.getByUsername(principal.getName()).getId());
        scStationInfoService.save(stationInfo);
        return Result.success(stationInfo);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sc:station:update')")
    public Result update(@Validated @RequestBody StationInfo stationInfo, Principal principal) {
        stationInfo.setAccountId(sysUserService.getByUsername(principal.getName()).getId());
        scStationInfoService.updateById(stationInfo);
        return Result.success(stationInfo);
    }
}
