package com.qcb.expresswrapping.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qcb.expresswrapping.common.Result;
import com.qcb.expresswrapping.entity.LogisticsInfo;
import com.qcb.expresswrapping.entity.StationExpress;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-11-17:16
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/sc/expresswrapping")
public class ScStationExpressController extends BaseController{

    @PostMapping("/list")
    @PreAuthorize("hasAnyAuthority('sc:express:list')")
    public Result list(@Validated @RequestBody StationExpress stationExpress, Principal principal){

        Page<StationExpress> pageData = scStationExpressService.page(getPage(), new QueryWrapper<StationExpress>()
                .eq("stationId", sysUserService.getByUsername(principal.getName()).getId())
                .like(StrUtil.isNotBlank(stationExpress.getOrderId()), "orderId", stationExpress.getOrderId())
                .ge(stationExpress.getEnterDate()!=null, "enterDate", stationExpress.getEnterDate())
                .ge(stationExpress.getLeaveDate()!=null, "leaveDate", stationExpress.getLeaveDate())
                .eq(stationExpress.getStatus()!=null, "status", stationExpress.getStatus())
                .orderByAsc("status")
        );

        return Result.success(pageData);
    }

    @PostMapping("/waitEntered")
    @PreAuthorize("hasAnyAuthority('sc:express:waitEntered')")
    public Result waitEntered(Principal principal){
        Page<LogisticsInfo> pageData = logisticsInfoService.page(getPage(), new QueryWrapper<LogisticsInfo>()
        .eq("targetSite", principal.getName())
        .eq("status", 1));
        return Result.success(pageData);
    }


}
