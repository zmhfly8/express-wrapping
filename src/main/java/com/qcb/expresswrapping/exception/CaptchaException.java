package com.qcb.expresswrapping.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2021-12-30-22:27
 * To change this template use File | Settings | File and Code Templates.
 */
public class CaptchaException extends AuthenticationException {

    public CaptchaException(String msg) {
        super(msg);
    }
}
