package com.qcb.expresswrapping.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Email;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-08-21:46
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class UserInfo {
    @TableId(value = "accountId")
    public Long accountId;
    @NotBlank(message = "用户名不能为空")
    public String name;
    public String sex;
    @TableField(value = "IDCard")
    @NotBlank(message = "身份证号码不能为空")
    public String IDCard;
    public String natives;
    @NotBlank(message = "联系方式不能为空")
    public String phone;
    @Email(message = "wrong email format")
    public String email;
}
