package com.qcb.expresswrapping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-12-23:03
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class OrderLogistics {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "orderId")
    private String orderId;
    @TableField(value = "logisticsId")
    private String logisticsId;
}
