package com.qcb.expresswrapping.entity.SysVo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 公共实体类
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-15-23:53
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private LocalDateTime created;
    private LocalDateTime updated;

    private Integer status;
}
