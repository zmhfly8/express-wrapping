package com.qcb.expresswrapping.entity.SysVo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-15-23:54
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "用户名不能为空")
    private String username;

    private String password;

    private String avatar;

    @TableField(exist = false)
    private List<SysRole> sysRoles = new ArrayList<>();
}
