package com.qcb.expresswrapping.entity.SysVo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-15-23:57
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysMenu extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /**
     * 父菜单ID，一级菜单为0
     */
    @NotNull(message = "上级菜单不能为空")
    private Long parentId;

    @NotNull(message = "菜单名称不能为空")
    private String name;

    private String path;

    /**
     * 授权(多个用逗号分隔，如：user:list,user:create)
     */
    @NotNull(message = "菜单授权码不能为空")
    private String perms;

    private String component;

    /**
     * 类型     0：目录   1：菜单   2：按钮
     */
    @NotNull(message = "菜单类型不能为空")
    private Integer type;

    private String icon;

    private Integer orderNum;

    private Integer status;

    @TableField(exist = false)
    private List<SysMenu> children = new ArrayList<>();
}
