package com.qcb.expresswrapping.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-08-22:04
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class StationInfo {
    @TableId(value = "account_id")
    public Long accountId;
    @NotBlank(message = "地域信息不能为空")
    public String area;
    @NotBlank(message = "详细地址不能为空")
    public String address;
    @NotBlank(message = "站点所有者信息不能为空")
    public String major;
    @NotBlank(message = "联系电话不能为空")
    public String phone;
    @Email(message = "wrong email format")
    public String email;
}
