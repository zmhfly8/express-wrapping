package com.qcb.expresswrapping.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-08-22:45
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class SendOrder {
    @TableId(value = "orderId")
    public String orderId;
    @TableField(value = "stationId")
    public long stationId;
    @TableField(value = "posName")
    public String posName;
    @TableField(value = "posArea")
    public String posArea;
    @TableField(value = "posAddress")
    public String posAddress;
    @TableField(value = "posPhone")
    public String posPhone;
    @TableField(value = "porName")
    public String porName;
    @TableField(value = "porArea")
    public String porArea;
    @TableField(value = "porAddress")
    public String porAddress;
    @TableField(value = "porPhone")
    public String porPhone;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "createDate")
    public Date createDate;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "updateDate")
    public Date updateDate;
    @TableField(value = "describes")
    public String describes;
    public Integer size;
    public float price;
    @TableField(value = "status")
    public Integer status;
    @TableField(exist = false)
    private List<LogisticsInfo> logisticsInfos;
}
