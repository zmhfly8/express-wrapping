package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.SysVo.SysRoleMenu;

public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
}
