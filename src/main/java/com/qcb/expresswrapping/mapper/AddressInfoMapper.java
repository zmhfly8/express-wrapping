package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.AddressInfo;

public interface AddressInfoMapper extends BaseMapper<AddressInfo> {
}
