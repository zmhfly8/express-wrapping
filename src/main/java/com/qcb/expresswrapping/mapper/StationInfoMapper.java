package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.StationInfo;

public interface StationInfoMapper extends BaseMapper<StationInfo> {
}
