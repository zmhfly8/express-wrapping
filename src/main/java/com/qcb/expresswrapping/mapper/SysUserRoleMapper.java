package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.SysVo.SysUserRole;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
