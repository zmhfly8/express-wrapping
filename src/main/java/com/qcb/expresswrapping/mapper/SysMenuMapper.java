package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.SysVo.SysMenu;

public interface SysMenuMapper extends BaseMapper<SysMenu> {
}
