package com.qcb.expresswrapping.common;

import lombok.Data;

/**
 * 封装统一结果集
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-16-20:37
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class Result {

    private int code;
    private String msg;
    private Object data;

    public static Result success(Object data){
        return success(200, "操作成功", data);
    }

    private static Result success(int code, String msg, Object data) {
        Result r = new Result();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

    public static Result fail(String msg){
        return fail(400, msg, null);
    }

    public static Result fail(int code, String msg, Object data){
        Result r = new Result();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

}
